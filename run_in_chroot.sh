#!/usr/bin/env bash

clear

# Figure out what cpu microarchitecture to install 
PS3='Select your CPU manufacturer(will install appropriate microcode later): '
cpus=("Intel" "AMD" "SKIP")
select cpu in "${cpus[@]}"; do
    case $cpu in
        "Intel")
            cpuarch=intel-ucode
            break
            ;;
        "AMD")
            cpuarch=amd-ucode
            break
            ;;
        "SKIP")
            echo "Will not install cpu microcode!"
            break
            ;;
        *) echo "Invalid option $REPLY";;
    esac
done

echo
# Figure out what gpu microarchitecture to install
PS3='Select your desired GPU driver: '
gpus=("Nvidia(proprietary)" "Nvidia(OSS)" "Nvidia-dkms" "AMD(OSS)" "SKIP") 
select gpu in "${gpus[@]}"; do
    case $gpu in
        "Nvidia(proprietary)")
            gpuDriver="nvidia nvidia-settings nvidia-utils"
            break
            ;;
        "Nvidia(OSS)")
            gpuDriver=xf86-video-nouveau
            break
            ;;
        "Nvidia-dkms")
            gpuDriver="nvidia-dkms nvidia-settings nvidia-utils"
            break
            ;;
        "AMD(OSS)")
            gpuDriver=xf86-video-amdgpu
            break
            ;;
        "SKIP")
            gpuDriver=none
            break
            ;;
        *) echo "Invalid option $REPLY";;
    esac
done

echo
echo "Optimising makepkg.conf and pacman"
sed -i 's/-march=[^ ]* -mtune=[^ ]*/-march=native/' /etc/makepkg.conf
sed -i "s/#MAKEFLAGS=\"-j2\"/MAKEFLAGS=\"-j\$(nproc)\"/" /etc/makepkg.conf
sed -i "s/!ccache/ccache/" /etc/makepkg.conf
sed -i "s/#Color/Color/" /etc/pacman.conf
sed -i "s/#Parallel Downloads = 5/Parallel Downloads = 5/" /etc/pacman.conf

echo
echo "Running reflector"
reflector --save /etc/pacman.d/mirrorlist --protocol https --latest 10 --sort rate
echo
echo "Installing ccache"
pacman -S --noconfirm ccache

echo
echo "Installing files with pacman (This will take a while !!!"
pacman -S $cpuarch refind snapper efibootmgr networkmanager network-manager-applet dialog wpa_supplicant mtools dosfstools reflector base-devel linux-headers xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils hplip alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack bash-completion bat openssh rsync reflector acpi acpi_call virt-manager qemu qemu-arch-extra edk2-ovmf bridge-utils dnsmasq vde2 openbsd-netcat iptables-nft ipset firewalld sof-firmware nss-mdns acpid os-prober ntfs-3g terminus-font


echo
echo "Please type in your desired username"
read -rp 'Username: ' userName
useradd -m "$userName"

echo
echo "Please choose your password"
passwd kellegram

echo
echo "Please choose root account password"
passwd

echo "$userName ALL=(ALL) ALL" >> /etc/sudoers.d/"$userName"

echo
echo "Enter your desired hostname"
read -rp "Hostname: " userHostname

echo
echo "Enter your keyboard layout"
read -rp "Keyboard layout: " userKeyLayout

echo
echo "Enter your locale (ie en_US)"
read -rp "Locale: " userLocale


echo
echo "Pulling your timezone and linking to localtime"
ln -sf /usr/share/zoneinfo/"$(curl -s http://ip-api.com/line?fields=timezone)" /etc/localtime

echo
echo "Activating the system clock"
hwclock --systohc

echo
echo "Applying your locale choices"
# US locale is needed for certain things, for example Steam
if [ ! "$userLocale" == "en_US"  ]; then
    echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
fi
echo "$userLocale.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=$userLocale.UTF-8" >> /etc/locale.conf

echo
echo "Applying your keymap"
echo "KEYMAP=$userKeyLayout" >> /etc/vconsole.conf

echo
echo "Applying your hostname"
echo "$userHostname" >> /etc/hostname
{
echo "127.0.0.1 localhost"
echo "::1       localhost"
echo "127.0.1.1 $userHostname.localdomain $userHostname"
} >> /etc/hosts


if [ ! $gpuDriver == "none" ]; then
    pacman -S --noconfirm $gpuDriver
fi

usermod -aG libvirt "$userName"

echo
echo "Enabling systemd services"
systemctl enable NetworkManager
systemctl enable sshd
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid


# refind installation from arch-chroot shamelessly *borrowed* from second2050 (https://github.com/second2050/arch-install-scripts/blob/dev/phase3_bootmgr_refind.sh)

mkinitcpio -P
# install refind
echo
echo "Installing refind..."

# install refind to the esp
refind-install

# setup up a working refind_linux.conf
echo "Creating refind config"
_uuid=$(lsblk -no UUID "$(df -P / | awk 'END{print $1}')")
{
    echo "\"Boot with standard options\"   \"root=UUID=$_uuid rw initrd=initramfs-%v.img loglevel=3 rd.udev.log_priority=3\""
    echo "\"Boot with fallback initramfs\" \"root=UUID=$_uuid rw initrd=initramfs-%v-fallback.img loglevel=3 rd.udev.log_priority=3\""
    echo "\"Boot to terminal\"             \"root=UUID=$_uuid rw initrd=initramfs-%v.img loglevel=3 rd.udev.log_priority=3 systemd.unit=multi-user.target\""
} > /boot/refind_linux.conf

# setup refind.conf
mv /boot/EFI/refind/refind.conf /boot/EFI/refind/refind.conf.old
cat <<EOF > /boot/EFI/refind/refind.conf
timeout 5
extra_kernel_version_strings linux-xanmod,linux-zen,linux-lts,linux
write_systemd_vars true
EOF

mv run_as_user_after_reboot.sh /home/"$userName"/
chown "$userName" /home/"$userName"/run_as_user_after_reboot.sh

mv install_xmonad_with_stack.sh /home/"$userName"/
chown "$userName" /home/"$userName"/install_xmonad_with_stack.sh

echo
echo "Script complete, please reboot and login as user (not ROOT!)"
echo "Then run run_as_user_after_reboot.sh"
echo "It has been copied for you to your home directory"