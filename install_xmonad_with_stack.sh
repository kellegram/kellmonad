#!/usr/bin/env bash

# Credit to Brian Buccola for instructions
# https://brianbuccola.com/how-to-install-xmonad-and-xmobar-via-stack/
# This script uses them as inspiration but is otherwise written by me (Kellegram).

create_xmonad_desktop () {
sudo tee -a /usr/share/xsessions/xmonad.desktop << EOF
[Desktop Entry]
Encoding=UTF-8
Type=Application
Name=XMonad
Comment=Lightweight X11 tiled window manager written in Haskell
Exec=xmonad
Icon=xmonad
Terminal=false
StartupNotify=false
Categories=Application;
EOF
}

if ! cd "$HOME" ; then
    echo "Failed to enter home folder"
    echo "Aborting"
    exit 1
fi


# Install basic packages
echo
echo "Installing basic packages (This will take a while, do not panic)..."
if ! paru -S --noconfirm polybar-git picom-jonaburg-git ttf-delugia-code xorg conky flameshot kitty lsd lightdm-webkit2-greeter lightdm-webkit2-theme-reactive lightdm firefox feh lxappearance thunar thunar-archive-plugin thunar-media-tags-plugin thunar-volman arandr simplescreenrecorder alsa-utils vlc dina-font tamsyn-font bdf-unifont ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid gnu-free-fonts ttf-ibm-plex ttf-liberation noto-fonts font-bh-ttf ttf-roboto tex-gyre-fonts ttf-ubuntu-font-family ttf-anonymous-pro ttf-cascadia-code ttf-fantasque-sans-mono ttf-fira-mono ttf-hack ttf-fira-code ttf-inconsolata ttf-jetbrains-mono ttf-monofur adobe-source-code-pro-fonts cantarell-fonts inter-font ttf-opensans gentium-plus-font ttf-junicode adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts noto-fonts-cjk noto-fonts-emoji ttf-font-awesome awesome-terminal-fonts rofi dunst
then
    echo "Installing basic packages failed, perhaps your internet connection is poor?"
    echo "Try re-running the script, if it persists, try running reflector again."
    exit
fi

echo
echo "Setting up lightdm"
sudo systemctl enable lightdm
# Set default lightdm-webkit2-greeter theme to reactive
sudo sed -i 's/^webkit_theme\s*=\s*\(.*\)/webkit_theme = reactive #\1/g' /etc/lightdm/lightdm-webkit2-greeter.conf
sudo sed -i 's/^debug_mode\s*=\s*\(.*\)/debug_mode = true #\1/g' /etc/lightdm/lightdm-webkit2-greeter.conf
# Set default lightdm greeter to lightdm-webkit2-greeter
sudo sed -i "s/#greeter-session=example-gtk-gnome/greeter-session=lightdm-webkit2-greeter/" /etc/lightdm/lightdm.conf


# Install stack-static, it comes with no haskell dependencies
echo "Installing stack-static and needed deps"
paru -S --noconfirm stack-static libxss &> /dev/null


if [ ! -d "$HOME/.stack" ]
then
    echo ".stack folder does not exist. Running 'stack setup'"
    stack setup
else
    if [ ! -f "$HOME/.stack/config.yaml" ]
    then
        echo "Stack config file does not exist, installing stack."
        stack setup
    else
        echo ".stack appears to be installed, skipping 'stack setup'"
    fi
fi


if [ ! -d "$HOME/.xmonad" ]
then
    echo ".xmonad folder does not exist, creating it."
    mkdir "$HOME/.xmonad"
    # this means no user xmonad.hs was present
    no_xmonad_user_config=true
else
    echo ".xmonad folder exists"
    if [ ! -f "$HOME/.xmonad/xmonad.hs" ] 
    then
        echo "No user configuration detected for xmonad"
        no_xmonad_user_config=true
    fi
fi

# Enter the .xmonad folder and pull  latest versions of xmonad and contrib
# then initialise stack for them

cd "$HOME/.xmonad" || { echo"Failed to enter .xmonad"; exit; }

git clone "https://github.com/xmonad/xmonad" xmonad-git
git clone "https://github.com/xmonad/xmonad-contrib" xmonad-contrib-git
# If you add xmobar here you need to remember to edit the stack.yaml created
# by stack init to include all extensions for xmobar otherwise many of its features will
# not work. That setup is not supported by me as it required regular manual intervention in stack.yaml

# Work around a nonsense randomly re-occuring bug
sed -i "s/,(\"C compiler flags\", \"\")/,(\"C compiler flags\", \"-fPIC -no-pie\")/" $HOME/.stack/programs/x86_64-linux/ghc-tinfo6-8.10.7/lib/ghc-8.10.7/settings
sed -i "s/,(\"C compiler link flags\", \"-fuse-ld=gold\")/,(\"C compiler link flags\", \"-fuse-ld=gold -no-pie\")/" $HOME/.stack/programs/x86_64-linux/ghc-tinfo6-8.10.7/lib/ghc-8.10.7/settings
sed -i "s/,(\"C compiler supports -no-pie\", \"NO\")/,(\"C compiler supports -no-pie\", \"YES\")/" $HOME/.stack/programs/x86_64-linux/ghc-tinfo6-8.10.7/lib/ghc-8.10.7/settings
stack init

# Install everything
if stack install
then
    echo "Stack install succeded"
else
    echo "Stack install failed, aborting"
    exit
fi

case ":$PATH:" in
  *:$HOME/.local/bin:*) echo "$HOME/.local/bin is in the path";;
  *) echo "Binary not in the path, please add it to your shell configuration file" 
     echo "Temporarily adding it to the path"
     export PATH=$PATH:$HOME/.local/bin
  ;;
esac

{
echo "# ~/.xmonad/build"
echo "#!/bin/sh"
echo "exec stack ghc -- \
  --make xmonad.hs \
  -i \
  -ilib \
  -fforce-recomp \
  -main-is main \
  -v0 \
  -o \"\$1\""
} >> build

chmod a+x build

# Attempting to recompile xmonad
if ! xmonad --recompile
then
    echo
    echo "xmonad failed to compile, you might need to log out and back in and try again"
fi

if [ ! -d /usr/share/xsessions ]
then
    sudo mkdir -p /usr/share/xsessions
fi

if [ ! -f /usr/share/xsessions/xmonad.desktop ]
then
    echo "xmonad.desktop file does not exist"
    echo "If you use a login manager like lightdm it might not see xmonad"
    echo "Would you like to create it? (Needs sudo permission)"
    PS3="Create the file (with sudo) [Yes/No]"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) create_xmonad_desktop; break;;
            No ) break ;;
        esac
    done
fi

if [ $no_xmonad_user_config ]
then
    echo "You do not have a custom xmonad.hs file"
    echo "If you do not add any, it will use defaults (you will be met with a black screen on reboot)"
fi

echo ""
echo "Xmonad installed successfully"
echo "It might not work 100% yet, it should after re-log/reboot"
echo "If it doesn't, create an issue in my gitlab"
