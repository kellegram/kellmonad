-------------------------------------------------------------------------------------------------------------
--  Kellmonad
-------------------------------------------------------------------------------------------------------------
--  Xmonad + Polybar config by Kellegram
--  https://kellegram.xyz


module Main (main) where

-------------------------------------------------------------------------------------------------------------
--  Imports
-------------------------------------------------------------------------------------------------------------
--  All imports, sorted by category, go here.

--  Base imports --------------------------------------------------------------

import System.Exit
import XMonad
import System.IO (hPutStrLn)
import qualified XMonad.StackSet as W
import XMonad.Config.Desktop  -- Better defaults, already setting a few things up that pretty much everyone does

--  Actions imports -----------------------------------------------------------
import XMonad.Actions.CopyWindow
import XMonad.Actions.CycleWS
import XMonad.Actions.WithAll (killAll, sinkAll)
import qualified XMonad.Actions.Search as S
import XMonad.Actions.GridSelect

--  Data imports --------------------------------------------------------------
import Data.Maybe (maybeToList, isJust)
import qualified Data.Map as M
import Data.Monoid
import Data.Char (isSpace, toUpper)
import Data.Tree
import Data.Maybe (isJust)
--  Graphics imports ----------------------------------------------------------
-- Will go here once they are needed ;)

--  Hooks imports -------------------------------------------------------------
import XMonad.Hooks.DynamicLog (statusBar, dynamicLogWithPP, wrap, xmobarPP, filterOutWsPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.ManageDocks (avoidStruts, docks, docksEventHook, ToggleStruts(..))
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageHelpers (doCenterFloat, isDialog ,isFullscreen, doFullFloat, composeOne, transience, (-?>))
import XMonad.Hooks.SetWMName
-- EDITME if using the built-in method of managing a bar (for example if you plan to use Xmobar instead)
-- import XMonad.Hooks.StatusBar
-- import XMonad.Hooks.StatusBar.PP

--  Prompt imports ------------------------------------------------------------
import XMonad.Prompt
import XMonad.Prompt.ConfirmPrompt
import XMonad.Prompt.Shell
import XMonad.Prompt.FuzzyMatch
import Control.Arrow (first)


--  Layout imports ------------------------------------------------------------
import XMonad.Layout.Renamed
import XMonad.Layout.Tabbed
import XMonad.Layout.LayoutModifier
import XMonad.Layout.SubLayouts
import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders
import XMonad.Layout.Dwindle
import XMonad.Layout.Simplest
import XMonad.Layout.Named
import XMonad.Layout.MultiToggle
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.Reflect
import XMonad.Layout.PerWorkspace
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.ResizableTile
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))
import XMonad.Layout.WindowNavigation

--  Utility imports -----------------------------------------------------------
import XMonad.Util.EZConfig(additionalKeysP)
import XMonad.Util.SpawnOnce
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.NamedScratchpad
import XMonad.Util.ClickableWorkspaces




-----------------------------------------------------------------------------------------------------------
-- Basic constants
-----------------------------------------------------------------------------------------------------------
-- These can be used in the main function and anywhere else in the config for convenience as well as
-- easy maintenance and customization

-- !IMPORTANT! -- here you choose 

basedConfig = desktopConfig 

-- Choose the fonts
myFont = "xft:Delugia Mono Nerd Font:bold:size=16:antialias=true:hinting=true"

-- myXmobarFont = "xft:Hack Nerd Font:bold:size=16:antialias=true:hinting=true"

-- Choose which terminal to use in the config
myTerminal = "kitty "

-- Set the focus to follow mouse
myFocusFollowsMouse = True

-- Make it so that clicking only focuses the windows and doesn't actually pass input
myClickJustFocuses = False

-- Set width of the window borders
myBorderWidth = 3

-- Set mod key to Super
myModMask = mod4Mask

-- Set focused and unfocused border colors
myNormalBorderColor  = "#515251"
myFocusedBorderColor = colorCyan

-- Set a favourite browser for use in the config (note I already added space after browser name)
myBrowser = "firefox "

-- Set text/code editor of choice to open things in
myEditor = "code "

-- For clarity
myRestartString = "xmonad --recompile || error \"Failed to recompile Xmonad\"; xmonad --restart || error \"Failed to restart Xmonad\""


-------------------------------------------------------------------------------------------------------------
--  My Pastel theme
-------------------------------------------------------------------------------------------------------------
--  This is a theme I created and use throughout the config, feel free to change the constants

colorFg             = "#E6EAE9"
colorFgBrighter     = "#fffffb"
colorBg             = "#242424"
colorBgBrighter     = "#303030"
colorRed            = "#E89292"
colorRedBrighter    = "#ffbebe"
colorOrange         = "#E8BD92"
colorOrangeBrighter = "#ffdeb6"
colorYellow         = "#E8E892"
colorYellowBrighter = "#fdffc6"
colorGreen          = "#A4E892"
colorGreenBrighter  = "#d3ffca"
colorCyan           = "#92E8cd"
colorCyanBrighter   = "#adfff0"
colorBlue           = "#92C7E8"
colorBlueBrighter   = "#beddff"
colorViolet         = "#BD92E8"
colorVioletBrighter = "#c8c2ff"
colorPink           = "#E892DB"
colorPinkBrighter   = "#ffd2ff"
colorGray           = "#c3d3c3"
colorGrayBrighter   = "#92a892"
colorCream          = "#f3e5c0"
colorCreamBrighter  = "#ecd69a"



-------------------------------------------------------------------------------------------------------------
--  Manage hook
-------------------------------------------------------------------------------------------------------------
--  

myManageHook = composeOne
  -- Handle floating windows:
  [ transience            -- move transient windows to their parent
  , isDialog              -?> doCenterFloat
  ] <+> composeAll
  [ className =? "confirm"         --> doFloat
  , className =? "file_progress"   --> doFloat
  , className =? "dialog"          --> doFloat
  , className =? "download"        --> doFloat
  , className =? "error"           --> doFloat
  , className =? "Gimp"            --> doFullFloat
  , className =? "MEGAsync"        --> doFloat
  , className =? "notification"    --> doFloat
  , className =? "pinentry-gtk-2"  --> doFloat
  , className =? "splash"          --> doFloat
  , className =? "toolbar"         --> doFloat
  -- , title =? "Library"             --> doFloat
  -- , title =? "Steam - News *"      --> doFloat
  , title =? "Oracle VM VirtualBox Manager"  --> doFloat
  , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
  , isFullscreen -->  doFullFloat
  ]

-------------------------------------------------------------------------------------------------------------
--  Startup hook
-------------------------------------------------------------------------------------------------------------
--  Anything that you want to be ran when xmonad is started goes here

myStartupHook :: X ()
myStartupHook = do
  spawnOnce "lxsession &"
  spawn     "$HOME/.config/polybar/launch.sh"
  spawnOnce "picom --experimental-backends &" --start up picom, experimental-backends will eventually phase out old backends
  spawnOnce "dunst &"
  spawnOnce "feh --bg-fill --randomize ~/Pictures/Wallpapers/*" --restore wallpaper
  spawnOnce "volumeicon &"
  spawnOnce "flameshot &"
  setWMName "LG3D" -- This is to fix certain Java applications. If you don't use any you might want to comment this out. 

-------------------------------------------------------------------------------------------------------------
--  Layout hook
-------------------------------------------------------------------------------------------------------------
--  

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = "xft:Delugia Mono Nerd Font:bold:size=13:antialias=true:hinting=true"
                 , activeColor         = colorGreen
                 , inactiveColor       = colorBg
                 , activeBorderColor   = "#46d9ff"
                 , activeBorderWidth   = 0
                 , inactiveBorderColor = "#282c34"
                 , inactiveBorderWidth = 0
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = colorFg
                 }
--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Define layouts and their modifiers here, apply in the hook

tiled   = named "Tall"
        $ mySpacing 8
        $ ResizableTall 1 (3/100) (1/2) []

grid    =  mySpacing 8
        $ Grid (16/10)

tabs    =  named "Tabs"
        $ tabbed shrinkText myTabTheme

bsp     = named "BSP"
        $ mySpacing 8
        $ emptyBSP

myLayoutHook =
  -- Automatically avoid stuff like status bars
  avoidStruts $

  -- Add the ability to toggle transforms dynamically
  mkToggle1 NBFULL $ 
  mkToggle1 REFLECTX $ 
  mkToggle1 REFLECTY $ 
  mkToggle1 NOBORDERS $
  mkToggle1 MIRROR $  

  -- Make borders disappear for fullscreen windows
  smartBorders $

  -- Make the talk workspace start in tabbed layout
  onWorkspace "talk" (tabs ||| Full) $

  -- Add the rest of the layouts here
  tiled   |||
  grid    |||
  bsp

-----------------------------------------------------------------------------------------------------------
-- Scratch pads                                                                                          --
-----------------------------------------------------------------------------------------------------------

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "cmus" spawnCmus findCmus manageCmus
                ]
  where
    spawnTerm  = myTerminal ++ "-T=scratchpad"
    findTerm   = title =? "scratchpad"
    manageTerm = defaultFloating
    spawnCmus  = myTerminal ++ "cmus -T=cmus"
    findCmus   = title =? "cmus"
    manageCmus = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w

-----------------------------------------------------------------------------------------------------------
-- Grid select                                                                                           --
-----------------------------------------------------------------------------------------------------------


myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
                  (0x28,0x2c,0x34) -- lowest inactive bg
                  (0x28,0x2c,0x34) -- highest inactive bg
                  (0xc7,0x92,0xea) -- active bg
                  (0xc0,0xa7,0x9a) -- inactive fg
                  (0x28,0x2c,0x34) -- active fg

-- gridSelect menu layout
mygridConfig :: p -> GSConfig Window
mygridConfig colorizer = (buildDefaultGSConfig myColorizer)
    { gs_cellheight   = 40
    , gs_cellwidth    = 200
    , gs_cellpadding  = 6
    , gs_originFractX = 0.5
    , gs_originFractY = 0.5
    , gs_font         = myFont
    }


spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = def
                   { gs_cellheight   = 40
                   , gs_cellwidth    = 200
                   , gs_cellpadding  = 6
                   , gs_originFractX = 0.5
                   , gs_originFractY = 0.5
                   , gs_font         = myFont
                   }

myAppGrid = [ ("Audacity", "audacity")
                 , ("Deadbeef", "deadbeef")
                 , ("Emacs", "emacsclient -c -a emacs")
                 , ("Firefox", "firefox")
                 , ("Geany", "geany")
                 , ("Geary", "geary")
                 , ("Gimp", "gimp")
                 , ("Kdenlive", "kdenlive")
                 , ("LibreOffice Impress", "loimpress")
                 , ("LibreOffice Writer", "lowriter")
                 , ("OBS", "obs")
                 , ("PCManFM", "pcmanfm")
                 ]

-----------------------------------------------------------------------------------------------------------
-- Search                                                                                                --
-----------------------------------------------------------------------------------------------------------

-- Define a search prompt. Some search engines are included in the library (Prefixed with "S.")
-- but any can be added, as long as a URL that expects a search term at the end exists

-- Declare some additional search engines
archwiki, udict :: S.SearchEngine

archwiki = S.searchEngine "archwiki" "https://wiki.archlinux.org/index.php?search="
udict    = S.searchEngine "urbandict" "https://www.urbandictionary.com/define.php?term="

-- Define a character used with a keybind to pick which search prompt should open
searchList :: [(String, S.SearchEngine)]
searchList = [ ("a", archwiki)
             , ("d", S.duckduckgo)
             , ("g", S.google)
             , ("i", S.images)
             , ("w", S.wayback)
             , ("u", udict)
             , ("w", S.wikipedia)
             , ("y", S.youtube)
             , ("z", S.amazon)
             ]


-----------------------------------------------------------------------------------------------------------
-- Custom XMonad Prompt                                                                                  --
-----------------------------------------------------------------------------------------------------------
myXPConfig :: XPConfig
myXPConfig = def
      { font                = myFont -- might want to change it depending on screen resolution
      , bgColor             = colorBg
      , fgColor             = colorCream
      , bgHLight            = colorCream
      , fgHLight            = colorBg
      , borderColor         = colorBlue -- The color doesn't matter, this border sucks
      , promptBorderWidth   = 0 -- Hidden, see above
      , promptKeymap        = myXPKeymap -- pass the self-defined keybinds
      , position            = Top
      , height              = 20
      , historySize         = 256
      , historyFilter       = id
      , defaultText         = []
      , autoComplete        = Just 100000
      , showCompletionOnTab = False
      , searchPredicate     = fuzzyMatch
      , defaultPrompter     = id $ map toUpper
      , alwaysHighlight     = True
      , maxComplRows        = Nothing
      }

-- Copy myXPConfig, but disable autocomplete, some prompts might not work great with it
myXPConfigNoAutoComplete :: XPConfig
myXPConfigNoAutoComplete = myXPConfig
      { autoComplete        = Nothing
      }

-- Define some keybinds for use in the prompts, as otherwise they will accept all keys as input
myXPKeymap :: M.Map (KeyMask,KeySym) (XP ())
myXPKeymap = M.fromList $
     map (first $ (,) controlMask)   -- control + <key>
     [ (xK_BackSpace, killWord Prev) -- kill the previous word
     , (xK_v, pasteString)           -- paste a string
     , (xK_q, quit)                  -- quit out of prompt
     , (xK_Return, setSuccess True >> setDone True)
     ]


-----------------------------------------------------------------------------------------------------------
-- Xmobar                                                                                                --
-----------------------------------------------------------------------------------------------------------

myWorkspaces :: [String]
myWorkspaces = ["term", "talk", "www", "code", "git", "file", "gaming"]

-- myWorkspaces = [" \59285 ", " \63097 ", " \63288 ", " \63079 ", " \59173 ", " \63306 ", " \61723 "]

-- myXmobarPP  :: PP
-- myXmobarPP  = def
--   { ppCurrent = xmobarColor colorOrangeBrighter "" . xmobarBorder "Bottom" myFocusedBorderColor 1 -- Current workspace in xmobar
--   , ppVisible = xmobarColor colorCyanBrighter ""                                                  -- Visible but not current workspace
--   , ppHidden  = xmobarColor colorBlueBrighter ""                                                  -- Hidden workspaces in xmobar
--   , ppHiddenNoWindows = xmobarColor colorGray ""                                                -- Hidden workspaces (no windows)
--   , ppTitle   = xmobarColor colorVioletBrighter "" . shorten 70                                   -- Title of active window in xmobar
--   , ppUrgent  = xmobarColor colorPinkBrighter "" . wrap "!" "!"                                   -- Urgent workspace
--   , ppSep     = " | "
--   , ppExtras  = []
--   , ppOrder   = \(ws:l:t) -> ws:l:t
--   }

-- xbar1 = statusBarPropTo "_XMONAD_LOG_1" "xmobar -x 0 $HOME/.config/xmobar/xmobarrc0"  (clickablePP myXmobarPP)
-- xbar2 = statusBarPropTo "_XMONAD_LOG_2" "xmobar -x 1 $HOME/.config/xmobar/xmobarrc1"  (clickablePP myXmobarPP)


-------------------------------------------------------------------------------------------------------------
--  Keybindings
-------------------------------------------------------------------------------------------------------------
--  

rofi_launcher = spawn "rofi -no-lazy-grab -show drun -modi run,drun,window -theme $HOME/.config/rofi/launcher/style -drun-icon-theme \"candy-icons\" "

myKeys :: [(String, X ())]
myKeys =
        -- Basic launching
        [  ("M-t", spawn myTerminal)                                              -- launch a terminal
        ,  ("M-p", rofi_launcher)                                                 -- launch rofi
        ,  ("M-S-s", spawn "flameshot gui")                                       -- Screenshot with Flameshot gui
        -- Xmonad
        ,  ("C-m r", spawn myRestartString)                                       -- Recompile and restart xmonad
        ,  ("C-m q", io (exitWith ExitSuccess))                                   -- Quit xmonad
        -- Kill window/s
        ,  ("M-q", kill1)                                                         -- Close focused window
        ,  ("M-S-q", killAll)                                                     -- Kill all windows on current workspace
        -- Layout
        ,  ("M-<Space>", sendMessage NextLayout)                                  -- Rotate through the available layout algorithms
        ,  ("M-n", refresh)                                                       -- Resize viewed windows to the correct size
        ,  ("M-i", incWindowSpacing 4)
        ,  ("M-o", decWindowSpacing 4)
        ,  ("M-f", sendMessage (T.Toggle "floats"))                               -- Toggles the floats layout
        ,  ("M-b f", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts)
        ,  ("M-b b", sendMessage $ MT.Toggle NOBORDERS)  -- Toggles noborder
        ,  ("M-b s", sendMessage ToggleStruts)
        -- Master tiling layoutd
        ,  ("M-<Tab>", windows W.focusDown)                                       -- Move focus to the next window
        ,  ("M-j", windows W.focusUp)                                             -- Move focus to the next window
        ,  ("M-k", windows W.focusDown)                                           -- Move focus to the previous window
        ,  ("M-m", windows W.focusMaster)                                         -- Move focus to the master window
        ,  ("M-<Return>", windows W.swapMaster)                                   -- Swap the focused window and the master window
        ,  ("M-S-j", windows W.swapDown)                                          -- Swap the focused window with the next window
        ,  ("M-S-k", windows W.swapUp)                                            -- Swap the focused window with the previous window
        ,  ("M-h", sendMessage Shrink)                                            -- Shrink the master area
        ,  ("M-l", sendMessage Expand)                                            -- Expand the master area
        ,  ("M-,", sendMessage (IncMasterN 1))                                    -- Increment the number of windows in the master area
        ,  ("M-.", sendMessage (IncMasterN (-1)))                                 -- Deincrement the number of windows in the master area
            -- Grid Select (CTR-g followed by a key)
        , ("C-g g", spawnSelected' myAppGrid)                 -- grid select favorite apps
        , ("C-g t", goToSelected $ mygridConfig myColorizer)  -- goto selected window
        , ("C-g b", bringSelected $ mygridConfig myColorizer) -- bring selected window

        -- Floating
        ,  ("M1-f t", withFocused $ windows . W.sink)                             -- Push window back into tiling
        ,  ("M1-f a", sinkAll)                                                    -- Push ALL windows back into tiling
        -- Scratch pads
        ,  ("M-s t", namedScratchpadAction myScratchPads "terminal")
        ,  ("M-s c", namedScratchpadAction myScratchPads "cmus")
        -- Multimedia Keys
        , ("<XF86AudioPlay>", spawn (myTerminal ++ "mocp --play"))
        , ("<XF86AudioPrev>", spawn (myTerminal ++ "mocp --previous"))
        , ("<XF86AudioNext>", spawn (myTerminal ++ "mocp --next"))
        , ("<XF86AudioMute>",   spawn "amixer set Master toggle")
        , ("<XF86AudioLowerVolume>", spawn "amixer set Master 2%- unmute")
        , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 2%+ unmute")
        , ("<XF86MonBrightnessUp>", spawn "light -A 5")
        , ("<XF86MonBrightnessDown>", spawn "light -U 5")
        , ("M1-p", spawn "flameshot full -c")

        ]
        -- Append shortcuts for search prompt
        ++ [("M1-s " ++ k, S.promptSearch myXPConfigNoAutoComplete f) | (k,f) <- searchList ]
        ++ [("M1-S-s " ++ k, S.selectSearch f) | (k,f) <- searchList ]
            -- The following lines are needed for named scratchpads.
          where nonNSP          = WSIs (return (\ws -> W.tag ws /= "nsp"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "nsp"))


---------------------------------------------------------------------------------------------------------
-- Mouse bindings                                                                                      --
---------------------------------------------------------------------------------------------------------
myMouseBindings (XConfig {XMonad.modMask = modm}) =
  M.fromList $
    -- mod-button1, Set the window to floating mode and move by dragging
    [ ( (modm, button1), ( \w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster)),
      -- mod-button2, Raise the window to the top of the stack
      ( (modm, button2), (\w -> focus w >> windows W.shiftMaster)),

      -- mod-button3, Set the window to floating mode and resize by dragging
      ( (modm,button3), (\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster))

      -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]




-------------------------------------------------------------------------------------------------------------
-- Main function
-------------------------------------------------------------------------------------------------------------
-- This is where everything comes together. It inherits the basedConfig, 
-- which is set in the constants section at the top.
-- If you are using KDE or other DE, modify basedConfig accordingly. 

main :: IO ()
main = do
  xmonad $ docks $ ewmhFullscreen $ ewmh $ basedConfig
    { terminal            = myTerminal
    , modMask             = myModMask
    , borderWidth         = myBorderWidth
    , focusFollowsMouse   = myFocusFollowsMouse
    , clickJustFocuses    = myClickJustFocuses
    , workspaces          = myWorkspaces
    , normalBorderColor   = myNormalBorderColor
    , focusedBorderColor  = myFocusedBorderColor
    , manageHook          = myManageHook <+> manageHook basedConfig
    , handleEventHook     = docksEventHook <+> handleEventHook basedConfig
    , startupHook         = myStartupHook
    , mouseBindings       = myMouseBindings
    , layoutHook          = desktopLayoutModifiers myLayoutHook
    } `additionalKeysP` myKeys


-- Uncomment this and comment the previous main function if you want to use xmobar
-- This one is written with 2 bars in mind on 2 monitors, check the docks for StatusBar to learn more

-- main :: IO ()
-- main = do
--   xmonad $ docks $ ewmhFullscreen $ ewmh $ withSB (xbar1 <> xbar2) $ basedConfig
--     { terminal            = myTerminal
--     , modMask             = myModMask
--     , borderWidth         = myBorderWidth
--     , focusFollowsMouse   = myFocusFollowsMouse
--     , clickJustFocuses    = myClickJustFocuses
--     , workspaces          = myWorkspaces
--     , normalBorderColor   = myNormalBorderColor
--     , focusedBorderColor  = myFocusedBorderColor
--     , manageHook          = myManageHook <+> manageHook basedConfig
--     , handleEventHook     = docksEventHook <+> handleEventHook basedConfig
--     , startupHook         = myStartupHook
--     , mouseBindings       = myMouseBindings
--     , layoutHook          = desktopLayoutModifiers myLayoutHook
--     } `additionalKeysP` myKeys