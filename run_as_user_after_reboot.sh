#!/usr/bin/env bash

clear

if [ "$EUID" == 0 ]
  then echo "Please do NOT run as root"
  exit
fi


# Install paru
echo "Installing paru (this will take a while, do not panic)..."
sudo pacman -S --noconfirm --needed base-devel &> /dev/null
git clone https://aur.archlinux.org/paru &> /dev/null
cd paru || { echo "Failed to enter paru/"; exit; }
makepkg -si &> /dev/null
cd "$HOME" || { echo "Failed to enter $HOME"; exit; }
sudo rm -rf paru/
echo "Done installing paru"

echo
echo "Installing zramd..."
paru -S zramd
sudo systemctl enable --now zramd.service
echo "Done installing zramd"
echo "You might want to change the zramd config if you have less RAM"

# Store current user again
userName=$(whoami)

# Snapper
echo
echo "Installing snapper related packages"
paru -S --noconfirm snap-pac snapper-gui refind-btrfs &> /dev/null 
echo "Setting up Snapper"
sudo umount /.snapshots
sudo rm -r /.snapshots
sudo snapper -c root create-config /
sudo btrfs subvolume delete /.snaphots
sudo mkdir /.snapshots
sudo mount -a
sudo chmod 750 /.snapshots
sudo sed -i "s/# ALLOW_USERS=""/ALLOW_USERS=\"$userName\"/" /etc/snapper/configs/root
sudo sed -i 's/# TIMELINE_LIMIT_HOURLY="10"/TIMELINE_LIMIT_HOURLY="5"/'  /etc/snapper/configs/root
sudo sed -i 's/# TIMELINE_LIMIT_DAILY="10"/TIMELINE_LIMIT_DAILY="7"/'  /etc/snapper/configs/root
sudo sed -i 's/# TIMELINE_LIMIT_MONTHLY="10"/TIMELINE_LIMIT_MONTHLY="0"/'  /etc/snapper/configs/root
sudo sed -i 's/# TIMELINE_LIMIT_YEARLY="10"/TIMELINE_LIMIT_HOURLY="0"/'  /etc/snapper/configs/root
echo "Finished setting up Snapper"
echo "Enabling snapper services"
sudo systemctl enable snapper-timeline.timer
sudo systemctl enable snapper-cleanup.timer
echo "Snapper fully installed"