#!/usr/bin/env bash

# For dialog
exec 3>&1

# Clear the screen and send a welcome
clear


# TODO probably useless since this would be acquired via git anyway
# if nc -zw1 google.com 443
# then
#     echo "You appear to have a working internet connection"
# else

#     echo "It appears you do not have an internet connection. \
#     # If you are absolutely sure that that's incorrect and you do have internet, \
#     # then you could continue, otherwise please abort"
# fi

# Install dialog, as well as its dependencies. This is crucial otherwise using
# old Arch ISOs will be impossible
if [[ ! -f /bin/dialog ]]; then
    pacman -Sy dialog bash ncurses
fi

sudo tee -a /etc/dialogrc << EOF
use_shadow = ON

screen_color = (RED,BLACK,ON)

# Shadow color
shadow_color = (BLACK,BLACK,OFF)

# Dialog box color
dialog_color = (WHITE,BLACK,OFF)

# Dialog box title color
title_color = (RED,BLACK,OFF)

# Dialog box border color
border_color = (BLACK,BLACK,ON)

# Active button color
button_active_color = (BLACK,RED,OFF)

# Inactive button color
button_inactive_color = (YELLOW,BLACK,OFF)

# Active button key color
button_key_active_color = (BLACK,RED,ON)

# Inactive button key color
button_key_inactive_color = (RED,BLACK,OFF)

# Active button label color
button_label_active_color = (WHITE,RED,OFF)

# Inactive button label color
button_label_inactive_color = (BLACK,BLACK,ON)

# Input box color
inputbox_color = (YELLOW,BLACK,OFF)

# Input box border color
inputbox_border_color = (YELLOW,BLACK,OFF)

# Search box color
searchbox_color = (YELLOW,BLACK,OFF)

# Search box title color
searchbox_title_color = (YELLOW,BLACK,ON)

# Search box border color
searchbox_border_color = (BLACK,BLACK,ON)

# File position indicator color
position_indicator_color = (RED,BLACK,OFF)

# Menu box color
menubox_color = (YELLOW,BLACK,OFF)

# Menu box border color
menubox_border_color = (BLACK,BLACK,ON)

# Item color
item_color = (WHITE,BLACK,OFF)

# Selected item color
item_selected_color = (BLACK,RED,OFF)

# Tag color
tag_color = (YELLOW,BLACK,OFF)

# Selected tag color
tag_selected_color = (BLACK,RED,OFF)

# Tag key color
tag_key_color = (RED,BLACK,OFF)

# Selected tag key color
tag_key_selected_color = (BLACK,RED,ON)

# Check box color
check_color = (YELLOW,BLACK,OFF)

# Selected check box color
check_selected_color = (BLACK,RED,ON)

# Up arrow color
uarrow_color = (RED,BLACK,OFF)

# Down arrow color
darrow_color = (RED,BLACK,OFF)
EOF

# Define the dialog exit status codes
: "${DIALOG_OK=0}"
: "${DIALOG_CANCEL=1}"
: "${DIALOG_HELP=2}"
: "${DIALOG_EXTRA=3}"
: "${DIALOG_ITEM_HELP=4}"
: "${DIALOG_ESC=255}"

# Define variables for dialog function arguments

: "${TITLE=1}"
: "${MENU=2}"
: "${SIZING=3}"
: "${OPTION1=4}"
: "${OPTION2=5}"
: "${OPTION3=6}"
: "${OPTION4=7}"

create_menu_dialog() {
    dialog \
    --title $TITLE \
    --menu $MENU $SIZING \
    $OPTION1
    $OPTION2
    $OPTION3
    $OPTION4

}



# Let user choose which disk they want to use for this installation

while [[ $SELECTED_DRIVE = "" ]]; do
    choice=$(dialog \
        --title "[DRIVE FORMATTING]" \
        --menu "Navigate with arrow keys or numbers" 0 0 4 \
        "1" "Display available drives" \
        "2" "Choose a drive to partition" \
        "3" "Currently selected drive path: $SELECTED_DRIVE" 2>&1 1>&3)
    
    case $? in
    "$DIALOG_CANCEL")
        clear
        echo "Script cancelled by user, aborting"
        exit
        ;;
    "$DIALOG_ESC")
        clear
        echo "Script failed, aborting"
        exit
        ;;
    esac

    # 
    case $choice in
    1)  dialog --title "Available drives" \
            --no-collapse --msgbox "$(lsblk -pd -o NAME,MODEL,SIZE,MOUNTPOINT | grep --color=never -E "MODEL|sd|vd|nvme")" 0 0
        ;;
    2)  SELECTED_DRIVE=$(dialog --title "Enter drive path to be used in the installation" \
            --no-collapse --inputbox "$(lsblk -pd -o NAME,MODEL,SIZE | grep -E "MODEL|sd|vd|nvme")" 0 0 "/dev/" 2>&1 1>&3)
        ;;
    3)  continue
        ;;
    esac
done


if [[ $(dialog \
    --title "Confirmation" \
    --menu "Are you sure you want to format and partition $SELECTED_DRIVE ?" 0 0 4 \
    "1" "Yes" \
    "2" "No" 2>&1 1>&3) = 2 ]]; then
    clear
    echo "Installation aborted by user"
    exit
fi

clear

# TODO Leave a bit of space at the end of the SSD for performance
echo "Partitioning the drive"
wipefs -af "$SELECTED_DRIVE" &> /dev/null
sgdisk -Zo "$SELECTED_DRIVE" &> /dev/null
sgdisk -n 0:0:+300M -t 0:ef00 -c 0:efi "$SELECTED_DRIVE" &> /dev/null
sgdisk -n 0:0:0 -t 0:8300 -c 0:storage "$SELECTED_DRIVE" &> /dev/null


# Make filesystems
echo "Partitioning complete. Making filesystems."

mkfs.vfat "$SELECTED_DRIVE"1 &> /dev/null
mkfs.btrfs "$SELECTED_DRIVE"2 &> /dev/null

echo "Mounting the drive"
mount "$SELECTED_DRIVE"2 /mnt

# Setup BTRFS subvolumes using the layout recommended by snapper
echo "Creating subvolumes"
btrfs su cr /mnt/@ &> /dev/null
btrfs su set-default /mnt/@ &> /dev/null
btrfs su cr /mnt/@home &> /dev/null
btrfs su cr /mnt/@snapshots &> /dev/null
btrfs su cr /mnt/@var_log &> /dev/null

umount /mnt

echo
echo "Remounting subvolumes on /mnt"
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@ "$SELECTED_DRIVE"2 /mnt
mkdir -p /mnt/{efi,home,.snapshots,var/log}
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@home "$SELECTED_DRIVE"2 /mnt/home
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@snapshots "$SELECTED_DRIVE"2 /mnt/.snapshots
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@var_log "$SELECTED_DRIVE"2 /mnt/var/log

echo
echo "Mounting the ESP partition"
mount "$SELECTED_DRIVE"1 /mnt/efi

echo
echo "Running reflector, this could take a moment depending on your location and internet!"
reflector --save /etc/pacman.d/mirrorlist --protocol https --latest 10 --sort rate &> /dev/null
echo
echo "Pacstraping base packages to /mnt (This might take a while)..."
pacstrap /mnt base base-devel dialog linux linux-firmware git curl nano btrfs-progs sudo &> /dev/null

echo "Generating fstab"
genfstab -U /mnt >> /mnt/etc/fstab

echo "Moving remaining installation scripts to chroot"

mkdir /mnt/kellmonad_scripts

# Declare an array of the remaining script file names, check if they exist and if yes, move them over to the new root
scriptFiles=("run_in_chroot.sh" "run_as_user_after_reboot.sh" "install_xmonad_with_stack.sh")

for scriptFile in "${scriptFiles[@]}"; do
    if test -f "$scriptFile"; then
        mv "$scriptFile" /mnt/
    else
        echo "$scriptFile is missing. You can find it in my repository at https://gitlab.com/kellegram/kellmonad"
    fi
done


dialog --title "New Arch root installed" \
            --no-collapse --msgbox "You are about to enter the new root on your selected drive. Please execute \"run_in_chroot.sh\" located in the kellmonad_scripts folder created in it." 0 0
clear
arch-chroot /mnt